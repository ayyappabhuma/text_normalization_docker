"""
The below code is adapted from celery's states.py
"""

#: State precedence.
#: None represents the precedence of an unknown state.
#: Lower index means higher precedence.
PRECEDENCE = [
    'SUCCESS',
    'PARTIAL',
    'FAILURE',
    'DELETED',
    None,
    'REVOKED',
    'STARTED',
    'QUEUED',
    'RECEIVED',
    'REJECTED',
    'RETRY',
    'PENDING',
]

#: Hash lookup of PRECEDENCE to index
PRECEDENCE_LOOKUP = dict(zip(PRECEDENCE, range(0, len(PRECEDENCE))))
NONE_PRECEDENCE = PRECEDENCE_LOOKUP[None]


def precedence(state):
    """Get the precedence index for state.
    Lower index means higher precedence.
    """
    try:
        return PRECEDENCE_LOOKUP[state]
    except KeyError:
        return NONE_PRECEDENCE


class state(str):
    """Task state.
    State is a subclass of :class:`str`, implementing comparison
    methods adhering to state precedence rules::
        >>> from celery.states import state, PENDING, SUCCESS
        >>> state(PENDING) < state(SUCCESS)
        True
    Any custom state is considered to be lower than :state:`FAILURE` and
    :state:`SUCCESS`, but higher than any of the other built-in states::
        >>> state('PROGRESS') > state(STARTED)
        True
        >>> state('PROGRESS') > state('SUCCESS')
        False
    """

    def __gt__(self, other):
        return precedence(self) < precedence(other)

    def __ge__(self, other):
        return precedence(self) <= precedence(other)

    def __lt__(self, other):
        return precedence(self) > precedence(other)

    def __le__(self, other):
        return precedence(self) >= precedence(other)


#: Task state is unknown (assumed pending since you know the id).
PENDING = 'PENDING'
#: Task was received by a worker (only used in events).
RECEIVED = 'RECEIVED'
#: Task was started by a worker (:setting:`task_track_started`).
STARTED = 'STARTED'
#: Task succeeded
SUCCESS = 'SUCCESS'
#: Task failed
FAILURE = 'FAILURE'
#: Task was revoked.
REVOKED = 'REVOKED'
#: Task was rejected (only used in events).
REJECTED = 'REJECTED'
#: Task is waiting for retry.
RETRY = 'RETRY'
#: Task is ignored
IGNORED = 'IGNORED'
#: Task results are deleted
DELETED = 'DELETED'
#: Task is queued and waiting for an available worker
QUEUED = 'QUEUED'
#: Task is in progress with at least one subtask completed(failure or success)
PARTIAL = 'PARTIAL'

READY_STATES = frozenset({SUCCESS, FAILURE, REVOKED, DELETED})
UNREADY_STATES = frozenset({PENDING, RECEIVED, STARTED, REJECTED, RETRY, QUEUED})
EXCEPTION_STATES = frozenset({RETRY, FAILURE, REVOKED})
PROPAGATE_STATES = frozenset({FAILURE, REVOKED})

ALL_STATES = frozenset({
    PENDING, RECEIVED, STARTED, SUCCESS, FAILURE, RETRY, REVOKED, QUEUED, DELETED, IGNORED, PARTIAL
})
