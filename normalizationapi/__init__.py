import connexion
from flask_ipban import IpBan

import os

app = connexion.FlaskApp(__name__, specification_dir='../')

flask_app = app.app

flask_app.config.from_object('config')
flask_app.config['UPLOAD_FOLDER'] = f'{flask_app.root_path}/{flask_app.config["RELATIVE_UPLOAD_FOLDER"]}'
flask_app.config['RESULT_FOLDER'] = f'{flask_app.root_path}/{flask_app.config["RELATIVE_RESULT_FOLDER"]}'

# upload folder and result folder from sparrowhawk's execution directory
flask_app.config['U_FOLDER'] = f'../../..{flask_app.root_path}/{flask_app.config["RELATIVE_UPLOAD_FOLDER"]}'
flask_app.config['R_FOLDER'] = f'../../..{flask_app.root_path}/{flask_app.config["RELATIVE_RESULT_FOLDER"]}'

flask_app.config['JSON_AS_ASCII'] = False

if not os.path.isdir(flask_app.config['UPLOAD_FOLDER']):
    os.makedirs(flask_app.config['UPLOAD_FOLDER'], exist_ok=True)

if not os.path.isdir(flask_app.config['RESULT_FOLDER']):
    os.makedirs(flask_app.config['RESULT_FOLDER'], exist_ok=True)

flask_app.config['IP_BAN_LIST_COUNT'] = 6
flask_app.config['IP_BAN_LIST_SECONDS'] = 3600

ip_ban = IpBan(flask_app)
ip_ban.load_nuisances()
ip_ban.ip_whitelist_add('127.0.0.1')
ip_ban.ip_whitelist_add('::')

app.add_api('openapi/text_normalization_api.yaml')
