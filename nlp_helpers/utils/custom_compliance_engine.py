from nlp_helpers.utils.custom_compliance import custom_compliance, multi_max


def get_data_object(data, compliance_config):
    rtn_object = []
    if 'status' in data and 'message' in data['status'] and 'segments' in data['status']['message']:
        for each_segment in data['status']['message']['segments']:
            if each_segment['channel'] in compliance_config:
                rtn_object.append(each_segment)
    return rtn_object


def match_score(match_conf, match_weight):
    return match_conf * match_weight


def seg_match_score(match_conf, match_weight, support_conf):
    return float((match_score(match_conf, match_weight) + (support_conf * 0.5)) / 1.5)


def get_conf(x):
    return x['Confidence']


def get_seg(x):
    return x['Segment']


def cc_scorer(config_json, data_object, language):
    config_match_list = []
    for nested_config in config_json['script']:
        segment_list = get_data_object(data_object, nested_config['channel_map'])
        match_weight = nested_config['weight']
        if nested_config['include_ranges'][0]:
            start_percent = nested_config['include_ranges'][0][0]
            end_percent = nested_config['include_ranges'][0][1]
        else:
            start_percent = 0
            end_percent = 100

        json_data = custom_compliance(nested_config['compliance_data'], segment_list,
                                      start_durpercent=start_percent, end_durpercent=end_percent, start_dur=0.0,
                                      end_dur=0.0, combine_len_thresh=10, lemmatize=True, combine_segs=False,
                                      combine_seg_by_len=False, language=language)

        if not json_data:
            continue

        matching_list, max_confidence = multi_max(json_data, get_conf, get_seg)
        if max_confidence == 0.0:
            continue

        # If we have more than one matching segments with maximum confidence score 
        if len(matching_list) > 1:
            seg_start_dur = 0.0
            seg_end_dur = segment_list[-1]['end']
            matchingseg_scorelist = []
            for item in matching_list:
                match_conf = max_confidence  # item['Confidence']
                start = item['start']
                end = item['end']
                for config in nested_config['subWorkFlow']:
                    supp_start = float(start) + config['config']['include_ranges'][0][0]
                    support_start = supp_start if supp_start > float(seg_start_dur) else float(seg_start_dur)
                    supp_end = float(end) + config['config']['include_ranges'][0][1]
                    support_end = supp_end if supp_end < float(seg_end_dur) else float(seg_end_dur)

                    support_data = custom_compliance(config['config']['compliance_data'],
                                                     segment_list, start_durpercent=0, end_durpercent=100,
                                                     start_dur=support_start, end_dur=support_end,
                                                     combine_len_thresh=10, lemmatize=True, combine_segs=False,
                                                     combine_seg_by_len=False, language=language)

                    if not support_data:
                        continue

                    max_suppconf = get_conf(max(support_data, key=get_conf))
                    if max_suppconf == 0.0:
                        continue

                    matchingseg_scorelist.append({
                        "Confidence": seg_match_score(match_conf, match_weight, max_suppconf),
                        "Segment": item
                    })
            # pp(matchingseg_scorelist)
            if matchingseg_scorelist:
                res1, max_matchconf = multi_max(matchingseg_scorelist, get_conf, get_seg)
                filtered_res1 = [item for n, item in enumerate(res1) if
                                 item not in res1[n + 1:]]
                for matchingseg in filtered_res1:
                    config_match_list.append({
                        "Confidence": max_matchconf,
                        "Segment": matchingseg,
                        "Config": nested_config['id']
                    })

        else:
            # TODO: Why no subworkflow confidence calculation over here
            config_match_list.append({
                "Confidence": max_confidence,
                "Segment": matching_list[0],
                "Config_Id": nested_config['id']
            })

    if not config_match_list:
        return []
    result, _ = multi_max(config_match_list, lambda x: x['Confidence'])
    return result
