from normalizationapi import flask_app
from utils import celery_instance
from celery.exceptions import Ignore
import subprocess
from werkzeug.utils import secure_filename
from tasks.tasker import taskize, TaskUpdates
import os
import uuid
import re


# function inputs a text document and converts numbers in it into text
@celery_instance.task(bind=True)
@taskize
def numbers_to_words(tasker: TaskUpdates, file):
    tasker.update_status(state=tasker.states.STARTED, message={'message': 'test'})
    try:
        output_filename = f'{uuid.uuid4()}_{secure_filename(file)}'
        print(output_filename)
        print(file)

        subprocess.call(
            f'normalizer_main --config=sparrowhawk_configuration.ascii_proto --multi_line_text < {flask_app.config["UPLOAD_FOLDER"]}/{file} 2> {flask_app.config["RESULT_FOLDER"]}/log > {flask_app.config["RESULT_FOLDER"]}/{output_filename}',
            shell=True, cwd="sparrowhawk-master/documentation/grammars/")

        err = ""
        regex = "ERROR"
        print(f'{flask_app.config["RESULT_FOLDER"]}/log')
        with open(f'{flask_app.config["RESULT_FOLDER"]}/log', 'r', encoding='ISO-8859-1') as e:
            print("inside result folder")
            for line in e:
                if re.search(regex, line):
                    err = err + line
            print("error is:", err)

        # os.remove(f'{flask_app.config["RESULT_FOLDER"]}/log')

        inp = ""
        with open(f'{flask_app.config["UPLOAD_FOLDER"]}/{file}', 'r', encoding='ISO-8859-1') as r:
            print("inside upload folder")
            for i in r.readlines():
                inp = inp + i
            print("input is:", inp)

        out = ""
        print(os.system('pwd'))
        with open(f'{flask_app.config["RESULT_FOLDER"]}/{output_filename}', 'r', encoding='ISO-8859-1') as fr:
            for i in fr.readlines():
                out = out + i

        # os.remove(f'{flask_app.config["RESULT_FOLDER"]}/{output_filename}')
        message = {'task_id': tasker.task_id, 'config': tasker.config,
                   'message': f'normalizing', 'input_message': inp
                   }

        # os.remove(f'{flask_app.config["UPLOAD_FOLDER"]}/{file}')

        tasker.update_status(state=tasker.states.STARTED, message=message)

    except Exception as exp:

        tasker.update_status(state=tasker.states.FAILURE, message={'error': str(exp)})
        raise Ignore()
    else:
        message['message'] = out
        message['err'] = err
        tasker.update_status(state=tasker.states.SUCCESS, message=message)
        return message
