from nlp_helpers.utils.custom_compliance_engine import cc_scorer


def nlp_cc_scored(compliance_conf, data_object, language):
    try:
        response = cc_scorer(compliance_conf, data_object, language)
        return True, {'result': response}
    except Exception as ex:
        response = {'result': [{'error': f"error generating nlp_custom_compliance {ex}"}]}
        print(f"error generating nlp_custom_compliance {ex}")
        return False, response
