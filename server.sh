#!/bin/bash
gunicorn -w 4 --bind 0.0.0.0:9901 wsgi:app --error-logfile app.log --capture-output --log-level debug
