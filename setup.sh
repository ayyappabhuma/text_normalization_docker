#!/bin/sh


# get optional arguments
port="9901"
while getopts ":p:" opt; do
  case $opt in
    p) port="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

## make sure docker has access to mount volume and the volumes exist

# check if the data folders exist in the mount space
redis_data_status=$(ls -la dolume/redis/|grep data)
rabbitmq_data_status=$(ls -la dolume/rabbitmq/|grep data)
if ! [ "$redis_data_status" ]
then
  echo 'creating data folder in redis mount space'
  mkdir dolume/redis/data
fi
if ! [ "$rabbitmq_data_status" ]
then
  echo 'creating data folder in rabbitmq mount space'
  mkdir dolume/rabbitmq/data
fi

# add access status to mounted volumes. No checks
sudo chmod +r dolume/redis/redis.conf dolume/rabbitmq/rabbitmq.conf
sudo chmod -R +wr dolume/redis/data dolume/rabbitmq/data
sudo chown -R 1001:1001 dolume/redis/data
sudo chmod -R g+rwX dolume/redis/data

# creates/empties the log file
# use touch if logs need to be presisted
echo > app.log
echo > backend.log

## build and run the docker services

# check status of queue and database docker
qdb_status=$(docker ps -a|grep qdb)
if ! [ "$qdb_status" ]
then
  # build queue and database docker
  echo "building queue and database docker"
  docker-compose -p qdb -f docker-compose-qdb.yml up -d
fi

# build running docker image for the api
docker build . -t tn_api
PORT=$port docker-compose -f docker-compose-web.yml up
