                                                                **Text Normalization** 

 

A Text-to-Speech synthesizer is a versatile system that correctly transforms most written data into human-like, natural speech. It operates on written, fully expanded words. Input text documents contain not only full words, such as milk and sugar, but also various other language units, such as numbers (15), dates (3/4/2003), acronyms (USA), abbreviations (i.e.), symbols ($), etc. All individual language units must be first consistently expanded into full words before they get synthesized. This conversion takes place internally within the synthesizer and is called text normalization. 

 

Text Normalization in Sparrowhawk is a two-step process: 1. Tokenization and Classification 2. Verbalization. The input text is first passed through a tokenizer, classifier which classifies tokens and simultaneously parses them into separate tokens. The tokens are then parsed into an internal data structure (a protocol buffer Token message), and collected into an utterance structure. The utterance is then passed to the verbalizer. All of the "interesting" tokens — things like numbers, money expression, times and so forth — are treated by the verbalizer grammar rules and converted into words. 

 

Tokenization is the process in which a whole sentence would be parsed into a sequence of tokens, where each token is either a semiotic class, punctuation or an ordinary token. For example, “he gave me $2.” becomes: 

tokens {name: "he"} 

tokens {name: "gave"} 

tokens {name: "me"} 

tokens {money {currency: "usd" amount { integer_part: "2"}}} 

tokens {name: "." pause_length: PAUSE_LONG phrase_break: true type: PUNCT} 

 

 

In the verbalizer, each token in an utterance is examined one by one from left to right. Ordinary words are left alone and punctuation tokens are examined and if the token is defined as a phrase-break, it is verbalized as "sil" (for "silence"). 

Semiotic classes have a more complex treatment. The message is serialized into an FST, which is then composed with the verbalizer rules defined in the verbalizer parameter file. However, in many cases it is desirable to serialize in various orders. Consider the case of money, where the input ordering after parsing is as above, e.g.: money {currency: "usd" amount { integer_part: "200"}}  

This reflects the written order ("$200") but we actually want to read this expression in the order number, then currency expression: "two hundred dollars". To handle this, Sparrowhawk serializes fields in all possible orders. It is then up to the grammar writer to write the verbalization grammars in such a way that one allows the field orders that are needed for the language. This lattice is then composed internally to Sparrowhawk with the verbalizer grammars. 

More details on tokenization and verbalization are available in Sparrowhawk documentation. 

 

All of the grammars for Sparrowhawk are written in the Thrax finite-state grammar development environment. In order to build Sparrowhawk the following packages must be installed first:  

    OpenFst 1.5.4 or higher (http://www.openfst.org) 

    Thrax 1.2.2 or higher (http://www.openfst.org/twiki/bin/view/GRM/Thrax) 

    re2 (https://github.com/google/re2) 

    protobuf (http://protobuf.googlecode.com/files/protobuf-2.5.0.tar.gz 

 

Finally, the Text Normalization API inputs a text file and gives a json response with input text, normalized text and error log of normalization. 

 

 

 

 

 