from bisect import bisect_left

from nltk.stem import WordNetLemmatizer


lemmatizer = WordNetLemmatizer()

# make sure no extra newlines in compliance_stopwords.txt
with open('nlp_helpers/utils/en_stopwords.txt', 'r') as f:
    en_stopwords = set(f.read().split('\n'))
with open('nlp_helpers/utils/hi_stopwords.txt', 'r', encoding='utf8') as f:
    hi_stopwords = set(f.read().split('\n'))


def multi_max(arr, key, get=lambda x: x):
    ans = []
    maxim = key(arr[0])  # 0
    for x in arr:
        if key(x) > maxim:
            ans = [get(x)]
            maxim = key(x)
        elif key(x) == maxim:
            ans.append(get(x))
    return ans, maxim


def overlap_metric(sample_wl, agent_wl):
    """
    Method to compute the overlap between two wordlists (sets)


    :param sample_wl: the sample word/phrase list to be compared against
    :param agent_wl: the agent segment's word/phrase list to be scored

    :return overlap: the word/phrase overlap score
    """

    # compute the overlap metric
    overlap = len(sample_wl & agent_wl) / len(sample_wl)
    # test that
    return overlap


def word_lemmatize(word):
    """
    Method to lemmatize the word and return the lemmatized word

    :param word: the non-lemmatized word

    :return lemmatized-word: returns a lemmatized word
    """
    return lemmatizer.lemmatize(word)


def generate_stem_words(word):
    """
        Method to stem the hindi word and return the root word

        :param word: the non-stemmed hindi word

        :return stemmed-word: returns a stem(root) word
        """
    suffixes = {
        1: [u"ो", u"े", u"ू", u"ु", u"ी", u"ि", u"ा"],
        2: [u"कर", u"ाओ", u"िए", u"ाई", u"ाए", u"ने", u"नी", u"ना", u"ते", u"ीं", u"ती", u"ता", u"ाँ", u"ां", u"ों",
            u"ें"],
        3: [u"ाकर", u"ाइए", u"ाईं", u"ाया", u"ेगी", u"ेगा", u"ोगी", u"ोगे", u"ाने", u"ाना", u"ाते", u"ाती", u"ाता",
            u"तीं", u"ाओं", u"ाएं", u"ुओं", u"ुएं", u"ुआं"],
        4: [u"ाएगी", u"ाएगा", u"ाओगी", u"ाओगे", u"एंगी", u"ेंगी", u"एंगे", u"ेंगे", u"ूंगी", u"ूंगा", u"ातीं", u"नाओं",
            u"नाएं", u"ताओं", u"ताएं", u"ियाँ", u"ियों", u"ियां"],
        5: [u"ाएंगी", u"ाएंगे", u"ाऊंगी", u"ाऊंगा", u"ाइयाँ", u"ाइयों", u"ाइयां"],
    }
    for L in 5, 4, 3, 2, 1:
        if len(word) > L + 1:
            for suf in suffixes[L]:
                if word.endswith(suf):
                    return word[:-L]
    return word


def get_ngrams(sent, ngram):
    """
    Splits into N-grams based on spaces -- it can be unigram or bigram or
    any number

    :param sent: a continuous text
    :param ngram: value for 'n' to split the text

    :return wordlist: a list of words after removing stopwords
    """
    output = []
    for i in range(len(sent) - ngram + 1):
        output.append(' '.join(sent[i:i + ngram]))
    return output


def get_ngram_chain(sent, lemmatize, rsw, language, max_ngram=3, min_ngram=1):
    """
    Get all the forms of n-grams from max_ngram in decreasing order

    :param sent: sentence to be split into multiple ngrams
    :param max_ngram: maximum ngram split required
    :param min_ngram: least ngram split required (default: unigram)
    :param lemmatize
    :param rsw: remove stopwords or not
    :param language: language of data and compliance config

    :return <listOfNgrams>: returns a list of all ngrams
    """
    min_ngram -= 1
    ret_list = []
    if language == 'en-US':
        sent = [word_lemmatize(x) for x in sent.split()] if lemmatize else sent.split()
    elif language == 'hi-IN':
        # sent = sent.split()
        sent = [generate_stem_words(x) for x in sent.split()] if lemmatize else sent.split()
    for i in range(max_ngram, min_ngram, -1):
        ret_list += get_ngrams(sent, i)
    if rsw:
        if language == 'en-US':
            return set(ret_list) - en_stopwords
        if language == 'hi-IN':
            return set(ret_list) - hi_stopwords
    else:
        return set(ret_list)


def find_closest(my_list, my_number):
    """
    Assumes myList is sorted. Returns closest value to myNumber.
    If two numbers are equally close, return the smallest number.

    :param my_list: a list of numbers
    :param my_number: number to be searched for from the list
    
    :return (closest_match, position): (<closest match from the list>, 
                                <position in the list>)
    """

    if len(my_list) == 0:
        return None, None

    pos = bisect_left(my_list, my_number)

    # print(my_list, pos, my_number)
    if pos == len(my_list):
        return my_list[-1], -1

    if my_list[pos] == my_number:
        return my_number, pos

    if pos == 0:
        return my_list[0], 0

    before = my_list[pos - 1]
    after = my_list[pos]

    if after - my_number < my_number - before:
        return after, pos
    else:
        return before, pos - 1


def combine_segs_by_len(asegment_list, sample_avg_len=20):
    recalculated_segs = []
    pointer, came_in = 0, False

    #    print('*********', asegment_list, '*********')
    for itr, segs in enumerate(asegment_list):

        #        print("Am here::", itr, pointer)
        if came_in and itr <= pointer:
            #            print(itr, pointer)
            continue

        if len(segs['data']['note'].split()) >= sample_avg_len:
            recalculated_segs.append(segs)
            came_in = False

        else:
            new_start, new_end = float(segs['start']), 0.0
            counter, temp_str = 0, ''

            for i in range(itr, len(asegment_list)):
                new_end = float(asegment_list[i]['end'])
                temp_str += asegment_list[i]['data']['note'] + ' '
                counter += 1
                if len(temp_str.split()) >= sample_avg_len or i == len(asegment_list) - 1:
                    recalculated_segs.append({'start': str(new_start),
                                              'end': str(new_end),
                                              'data': {'note': temp_str},
                                              'channel': asegment_list[i]['channel']})
                    break
            came_in = True
            pointer = itr + counter

    # print('##########', recalculated_segs, '##########')
    return recalculated_segs


def get_call_segs_by_percent(asegment_list, start_percent, end_percent, start_dur, end_dur, combine_segs=False):
    """
    Method to compute the total call duratioon and also the start
    and end indexes, possibly also a combined agent segment data

    :param asegment_list: the complete agent segment list
    :param start_percent: the start percent of call
    :param end_percent: the end percent of call
    :param start_dur: the start duration of call
    :param end_dur: the end duration of call
    :param combine_segs: return segments in a combined format or a list
    :return <agentsegment list between percents>:
    """
    # all segment time_stamps
    start_times = [float(x['start']) for x in asegment_list]
    end_times = [float(x['end']) for x in asegment_list]

    # when the length of the call is less than 2 segments
    # return the segment list as is
    if len(asegment_list) <= 1:
        return asegment_list

    # Get the total call duration
    call_duration = end_times[-1]  # float(asegment_list[-1]['end'])

    if start_dur == 0.0 and end_dur == 0.0:
        # get the start time and end time based on percentage 
        # start_dur, end_dur = 0.0, 0.0
        if start_percent > 0:
            start_dur = float(call_duration * start_percent) / float(100)
        end_dur = float(call_duration * end_percent) / float(100)

    start_time, start_idx = find_closest(start_times, start_dur)
    end_time, end_idx = find_closest(end_times, end_dur)
    if combine_segs:
        return [{
            'data': {'note': ' '.join(x['data']['note'] for x in asegment_list[start_idx:end_idx])},
            'start': asegment_list[start_idx]['start'],
            'end': asegment_list[end_idx]['end'],
            'channel': asegment_list[end_idx]['channel']
        }]
    return asegment_list[start_idx:end_idx + 1]


def custom_compliance(sample_sentlist, asegment_list, language, start_durpercent=0, end_durpercent=100, start_dur=0.0,
                      end_dur=0.0, combine_len_thresh=2, lemmatize=False, combine_segs=False, combine_seg_by_len=False):
    """
    Method to provide similarity metrics w.r.t custom compliance
    which are script based.


    :param sample_sentlist: a list of sample sentences provided by
    the Systems Integrator (SI) - which are considered to be samples
    of compliance
    :param asegment_list: a list of agent segments to be verified with
    :param language: language of data nd compliance config
    :param start_durpercent: start percent (will be converted to duration)
    :param end_durpercent: end percent (will be converted to duration)
    :param start_dur: start time
    :param end_dur: end time
    :param combine_len_thresh: dunno
    :param lemmatize: takes values True/False -- for lemmatization
    :param combine_segs: takes values True/False -- whether to combine the segments into one
    for combining them otherwise consider them individually
    :param combine_seg_by_len: takes values True/False -- whether to combine the
    segments by the length threshold or not

    :return <score-list>: scorelist for each asegment in the asegment_list
    """

    # all_segscores = []

    # Get only the relevant segments based on the start and end percents
    # if combine_segs or :
    asegment_list = get_call_segs_by_percent(asegment_list, start_durpercent,
                                             end_durpercent, start_dur, end_dur, combine_segs=combine_segs)

    if combine_seg_by_len:
        # definitely not the ideal # of words to combine segments
        # so combine by the average sample sentences length
        if combine_len_thresh == 2:
            add = 0
            for x in sample_sentlist:
                add += len(x.split())
            try:
                combine_len_thresh = add / len(sample_sentlist)
                print('Setting *combinelen_thresh* to {}'.format(combine_len_thresh))
            except ZeroDivisionError:
                combine_len_thresh = 10  # set the default to 10
                print('ZeroDivisionError exception, so setting the *combine_len_thresh* to {}'.format(
                    combine_len_thresh))

        asegment_list = combine_segs_by_len(asegment_list, combine_len_thresh)

    # first step is to get the unique list of words from the samples and segments
    # second step is to get n-grams from the samples and segments
    split_samples = [get_ngram_chain(x, lemmatize, True, language, 3, 1)
                     for x in sample_sentlist]

    split_segments = [get_ngram_chain(x['data']['note'], lemmatize, True, language, 3, 1)
                      for x in asegment_list]

    # third step is to compute the overlap metric and return the same
    # for every agent segment within the call-percent,
    ret_val = [
        {
            "Confidence": max([overlap_metric(sseg, segs)
                               for sseg in split_samples]),
            "Segment": actual
        }
        for segs, actual in zip(split_segments, asegment_list)
    ]
    return ret_val
