FROM ubuntu:18.04

WORKDIR /
RUN apt-get update
RUN apt-get install python3 python3-pip -y
RUN apt-get install git -y
RUN apt-get install wget -y
RUN apt-get install unzip -y
RUN apt-get install libtool-bin libprotobuf-dev libprotobuf-c-dev libprotoc-dev  protobuf-compiler libre2-dev -y
RUN apt-get install locales -y
RUN locale-gen en_US.UTF-8

WORKDIR /textnormalization-api/
# openfst
RUN wget http://www.openfst.org/twiki/pub/FST/FstDownload/openfst-1.6.8.tar.gz
# thrax
RUN wget http://www.openfst.org/twiki/pub/GRM/ThraxDownload/thrax-1.2.7.tar.gz
# sparrowhawk
RUN wget https://github.com/google/sparrowhawk/archive/master.zip -O sparrowhawk.zip
#extracting zip files
RUN tar -xf openfst-1.6.8.tar.gz
RUN tar -xf thrax-1.2.7.tar.gz
RUN unzip sparrowhawk.zip
#removing zip files
RUN rm openfst-1.6.8.tar.gz
RUN rm thrax-1.2.7.tar.gz
RUN rm sparrowhawk.zip	

WORKDIR /textnormalization-api/openfst-1.6.8/
RUN ./configure  --enable-far=true --enable-pdt=true --enable-mpdt=true --enable-grm
RUN make
RUN make install

WORKDIR /textnormalization-api/thrax-1.2.7/
RUN ./configure 
RUN make
RUN make install

WORKDIR /textnormalization-api/protobuf/
RUN protoc --version

WORKDIR /textnormalization-api/sparrowhawk-master/
RUN ./configure
RUN make
RUN make install
RUN ldconfig

WORKDIR /
RUN apt-get install python -y

WORKDIR /textnormalization-api/sparrowhawk-master/documentation/grammars/en_toy/classify
RUN thraxmakedep tokenize_and_classify.grm
RUN make

WORKDIR /textnormalization-api/sparrowhawk-master/documentation/grammars/en_toy/verbalize
RUN thraxmakedep verbalize.grm 
RUN make

WORKDIR /textnormalization-api/sparrowhawk-master/documentation/grammars/en_toy/verbalize_serialization
RUN thraxmakedep verbalize.grm
RUN make

WORKDIR /textnormalization-api/
COPY . /textnormalization-api/
RUN python3 -m pip install -r requirements.txt

RUN touch app.log
RUN touch backend.log
ENV LC_ALL="C.UTF-8"