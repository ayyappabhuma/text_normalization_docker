import base64
import hashlib
import json
from functools import wraps

import socketio
from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from Crypto.Util.Padding import pad

from tasks import states
from utils import celery_instance


class TaskUpdates(object):
    states = states

    def __init__(self, celery: celery_instance.Task, config, tracker, rsa_pub_key):
        self.logs = []
        self.task_id = celery.request.id
        if tracker:
            self.tracker = tracker
        else:
            self.tracker = self.task_id
        self.config = config
        self.celery = celery
        self.intermediate_results = self.config.get('intermediate_results', False)
        self.debug = self.config.get('debug', False)
        self.socket, self.socket_event, self.socket_error = None, None, None
        self.debug_socket, self.debug_socket_event, self.debug_socket_error = None, None, None
        self.set_socket()
        self.set_debug_socket()
        self.cipher_rsa, self.rsa_fin = self.generate_keys(rsa_pub_key)
        self.update_status(state=self.states.STARTED, message={'status': 'Task Started'})

    @staticmethod
    def rsa_to_fingerprint(rsakey):
        key = rsakey.exportKey()
        fp_plain = hashlib.md5(key).hexdigest()
        return ':'.join(a + b for a, b in zip(fp_plain[::2], fp_plain[1::2]))

    @staticmethod
    def generate_keys(rsa_pub_key):
        if not rsa_pub_key:
            return None, None
        rsa_key = RSA.import_key(rsa_pub_key)
        cipher_rsa = PKCS1_OAEP.new(rsa_key)
        rsa_fin = TaskUpdates.rsa_to_fingerprint(rsa_key)
        return cipher_rsa, rsa_fin

    def do_encoding(self, message):
        if not self.rsa_fin:
            return message
        if type(message) == dict:
            message = json.dumps(message).encode('utf-8')
        # cipher_aes state changes after each call to encrypt so it can't be up in generate_keys
        key = get_random_bytes(16)
        cipher_aes = AES.new(key, AES.MODE_CBC)
        enc_key = base64.b64encode(self.cipher_rsa.encrypt(key)).decode('ascii')
        iv = base64.b64encode(cipher_aes.iv).decode('ascii')
        encm = cipher_aes.encrypt(pad(message, 16))
        ciphertext = base64.b64encode(encm).decode('ascii')
        encoded_data = {'iv': iv, 'cipher': ciphertext, 'keys': {self.rsa_fin: enc_key}}
        return encoded_data

    def get_socket(self, socket_config):
        socket_error, socket = None, None
        socket_event = socket_config.get('socket_event', 'message')
        if socket_config:
            try:
                socket = socketio.Client()
                if 'socket' in socket_config:
                    socket_kwargs = {}
                    if 'socket_path' in socket_config:
                        socket_kwargs['socketio_path'] = socket_config['socket_path']
                    if 'socket_namespace' in socket_config:
                        socket_kwargs['namespaces'] = [socket_config['socket_namespace']]
                    socket.connect(socket_config['socket'], **socket_kwargs)
                    socket.sleep(1)
                    if 'socket_room' in socket_config and 'socket_room_event' in socket_config:
                        socket.emit(socket_config['socket_room_event'], socket_config['socket_room'],
                                    namespace=socket.namespaces[0])
                    socket.emit('add user', str(self.task_id), namespace=socket.namespaces[0])
            except Exception as exp:
                socket = None
                socket_error = str(exp)
                print('Socket Error', socket_error)

        return socket, socket_event, socket_error

    def set_socket(self):
        if 'socket' in self.config:
            self.socket, self.socket_event, self.socket_error = self.get_socket(self.config['socket'])
        if self.socket_error:
            self.config['socket']['socket_error'] = self.socket_error

    def set_debug_socket(self):

        if 'debug_socket' in self.config:
            self.debug_socket, self.debug_socket_event, self.debug_socket_error = self.get_socket(
                self.config['debug_socket'])
        if self.debug_socket_error:
            self.config['debug_socket']['socket_error'] = self.socket_error

    def emit_to_socket(self, message: dict):
        """
        Posts messages to socket if configured and are reachable
        :param message: Message (in dict format) to emit to socket
        :return:
        """
        try:
            if self.socket and not self.socket_error:
                self.socket.emit(self.socket_event, message, namespace=self.socket.namespaces[0])
        except Exception as exp:
            self.socket_error = f'WARNING: Exception occurred while posting to socket: {str(exp)}'
            print(self.socket_error)
            self.logs.append(self.socket_error)

        try:
            if self.debug_socket and not self.debug_socket_error:
                self.debug_socket.emit(self.debug_socket_event, message, namespace=self.debug_socket.namespaces[0])
        except Exception as exp:
            self.debug_socket_error = f'WARNING: Exception occurred while posting to debug socket: {str(exp)}'
            print(self.debug_socket_error)
            self.logs.append(self.debug_socket_error)

    def disconnect_socket(self):
        if self.debug_socket and not self.debug_socket_error:
            self.debug_socket.sleep(1)
            self.debug_socket.disconnect()
        if self.socket and not self.socket_error:
            self.socket.sleep(1)
            self.socket.disconnect()

    def update_status(self, state, message: dict):
        """
        Updates state of celery task along with the message
        :param state: state of celery task to be updated
        :param message: message/meta data of the celery task for the current state
        :return:
        """
        if 'tracker' not in message:
            message['tracker'] = self.tracker
        if 'log' not in message:
            message['log'] = self.logs
        else:
            message['log'].extend(self.logs)

        message = self.do_encoding(message)

        self.celery.update_state(state=state, meta=message)

        self.emit_to_socket(message)


def taskize(func):
    @wraps(func)
    def wrapper(celery_obj: celery_instance.Task, *args, **kwargs):
        config = {}
        tracker = None
        rsa_pub_key = None
        if 'config' in kwargs:
            config = kwargs['config']
            del kwargs['config']
        if 'tracker' in kwargs:
            tracker = kwargs['tracker']
            del kwargs['tracker']
        if 'rsa_key' in kwargs:
            rsa_pub_key = kwargs['rsa_key']
            del kwargs['rsa_key']
        task_updates = TaskUpdates(celery_obj, config, tracker, rsa_pub_key)
        print(func)
        return func(task_updates, *args, *kwargs)

    return wrapper
