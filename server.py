#!venv/bin/python

from normalizationapi import app

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=9901, debug=True)
